package com.example.football;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int homePoint = 0;
    int guestPoint = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (savedInstanceState != null) {
            homePoint = savedInstanceState.getInt("homePoint");
            guestPoint = savedInstanceState.getInt("guestPoint");
            TextView textViewLeft = (TextView) findViewById(R.id.textViewLeft);
            textViewLeft.setText(homePoint + " ");
            TextView textViewRight = (TextView) findViewById(R.id.textViewRight);
            textViewRight.setText(guestPoint + " ");
        }
    }

    public void leftButton(View view) {
        homePoint++;
        if (homePoint == Integer.MAX_VALUE) {
            homePoint = 0;
        }
        TextView textView = (TextView) findViewById(R.id.textViewLeft);
        textView.setText(homePoint + " ");
    }

    public void rightButton(View view) {
        guestPoint++;
        if (guestPoint == Integer.MAX_VALUE) {
            guestPoint = 0;
        }
        TextView textView = (TextView) findViewById(R.id.textViewRight);
        textView.setText(guestPoint + " ");
    }


    public void reset(View view) {
        guestPoint = 0;
        homePoint = 0;
        TextView textViewLeft = (TextView) findViewById(R.id.textViewLeft);
        textViewLeft.setText(homePoint + " ");
        TextView textViewRight = (TextView) findViewById(R.id.textViewRight);
        textViewRight.setText(guestPoint + " ");
    }


    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("homePoint", homePoint);
        savedInstanceState.putInt("guestPoint", guestPoint);
    }
}
